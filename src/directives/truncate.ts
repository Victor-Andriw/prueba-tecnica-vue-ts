import { DirectiveBinding } from 'vue'

const truncateText = (el: HTMLElement, binding: DirectiveBinding<number>) => {
  const maxLength = binding.value
  let text = el.textContent?.trim() ?? ''
  if (text.length > maxLength) {
    text = text.substring(0, maxLength) + '...'
  }
  el.textContent = text
}

export default truncateText