import { RouteRecordRaw, createRouter, createWebHistory } from "vue-router";
import PostList from "../components/PostList.vue";
import PostItem from "../components/PostItem.vue";
import UserDetails from "../components/UserDetails.vue";

import { Post } from "../interfaces/post";
import { User } from "../interfaces/users";
import usePostsData from "../composables/usePostsData";
import useUsersData from "../composables/useUsersData";
const { posts, getPosts } = usePostsData();
const { users, getUsers } = useUsersData();
getPosts();
getUsers();

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "posts",
    component: PostList,
  },
  {
    path: "/details/post/:id",
    name: "post",
    component: PostItem,
    props: (route) => {
      const post = posts.value.find(
        (p: Post) => p.id.toString() === route.params.id
      );
      return { post };
    },
  },
  {
    path: "/details/user/:id",
    name: "user",
    component: UserDetails,
    props: (route) => {
      const user = users.value.find(
        (u: User) => u.id.toString() === route.params.id
      );
      return { user };
    },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
