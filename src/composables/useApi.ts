import { ref } from "vue";
import axios from "axios";
import { Post } from "../interfaces/post";
import { User } from "../interfaces/users";
import { Comment } from "../interfaces/comments";

const BASE_URL = "https://jsonplaceholder.typicode.com";

export default function useApi() {
  const posts = ref<Post[]>([]);
  const users = ref<User[]>([]);
  const comments = ref<Comment[]>([]);

  const getPosts = async () => {
    const response = await axios.get<Post[]>(`${BASE_URL}/posts`);
    posts.value = response.data;
  }

  const getUsers = async () => {
    const response = await axios.get<User[]>(`${BASE_URL}/users`);
    users.value = response.data;
  }

  const getComments = async (id:number) => {
    const response = await axios.get<Comment[]>(`${BASE_URL}/posts/${id}/comments`);
    comments.value = response.data;
  }

  return {
    posts,
    users,
    comments,
    getPosts,
    getUsers,
    getComments,
  };
}
