import useApi from "./useApi";

export default function useUsersData() {
  const { users, getUsers } = useApi();

  const getAuthorName = ( userId: number ) => {
    const user = users.value.find((u) => u.id === userId);
    return user ? user.name : "Unknown";
  }

  return { users, getUsers, getAuthorName };
}
