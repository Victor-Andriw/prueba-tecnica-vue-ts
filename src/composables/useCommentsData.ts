import useApi from "./useApi";

export default function useCommentsData() {
  const { comments, getComments } = useApi();

  return { comments, getComments };
}
