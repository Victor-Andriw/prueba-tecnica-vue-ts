import useApi from "./useApi";

export default function usePostsData() {
  const { posts, getPosts } = useApi();

  return { posts, getPosts };
}
